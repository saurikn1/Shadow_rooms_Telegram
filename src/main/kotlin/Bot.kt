import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import org.telegram.telegrambots.exceptions.TelegramApiException
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.TelegramBotsApi
import org.telegram.telegrambots.api.methods.send.SendMessage
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText
import org.telegram.telegrambots.api.objects.Message
import org.telegram.telegrambots.api.objects.Update
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton


class SimpleBot : TelegramLongPollingBot() {

    var updateGlobal: Update? = null

    val gson = Gson()
    override fun getBotUsername(): String {
        return "all4funBot"
    }

    override fun getBotToken(): String {
        return "500969562:AAEi6F6E7BtQUaWWmBlqMEPbNIBCjIraGe8"
    }

    override fun onUpdateReceived(update: Update) {
        updateGlobal = update
        if(update.hasCallbackQuery()){
            val callData = gson.fromJson<List<String>>(update.callbackQuery.data)
            val chatId = callData[1]
            when (callData[0]){
                "Button1" -> {
                    val message = update.callbackQuery.message
                    drawForms(message, "You pressed the Button  1⃣")
            }
                "Button2" -> {
                    val message = update.callbackQuery.message
                    drawForms(message, "You pressed the Button  2⃣")
                }
            }
        }

        val message = update.message
        if (message != null && message.hasText()) {
            if (message.text == "/start"){
                drawForms(sendMsg(message, "Please wait")!!, "Приветствую тебя, ${message.from.firstName}, ну или как тебя называют в узких кругах - " +
                        "${message.from.userName}!" +
                        "\n Вы вошли в первую комнату, но она оказалась пуста. \n Удачной игры \uD83D\uDE08")
            }
            else
                sendMsg(message, "Прошу прощения, но я не понимаю эльфийский, о Великий!")
        }
    }


    fun drawForms(message: Message, text: String){
        val editMessage = EditMessageText()
        editMessage.messageId = message.messageId
        val keyboard = InlineKeyboardMarkup()
        val send = listOf<String>("Button1", message.chatId.toString())
        val send2 = listOf<String>("Button2", message.chatId.toString())
        val btn = InlineKeyboardButton().setText("Just test touching").setCallbackData(gson.toJson(send))
        val btn2 = InlineKeyboardButton().setText("Что-то невероятное").setCallbackData(gson.toJson(send2))
        editMessage.chatId = message.chatId.toString()
        val btnList  = mutableListOf(btn,btn2)
        val btnLists = mutableListOf<List<InlineKeyboardButton>>()
        btnLists.add(btnList)
        keyboard.keyboard = btnLists
        editMessage.replyMarkup = keyboard
        editMessage.text = text
        try {
            execute(editMessage)
        } catch (e: TelegramApiException) {
            e.printStackTrace()
        }
    }

    fun sendMsg(message: Message, text: String): Message? {
       val sendMessage = SendMessage()
        sendMessage.chatId = message.chatId.toString()
        sendMessage.text = text
        return execute(sendMessage)
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            ApiContextInitializer.init()
            val telegramBotsApi = TelegramBotsApi()
            try {
                telegramBotsApi.registerBot(SimpleBot())
                val test = BotInterface()
            } catch (e: TelegramApiException) {
                e.printStackTrace()
            }
        }
    }

}